<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\User::create([
            "id" => 1,
            "name" => "Jonathan E.",
            "lastname" => "Gonzalez N.",
            "username" => "admin",
            "phone_number" => "9991231233",
            "birthday" => "1995-03-15",
            "password" => "admin123",
            "terms_and_conditions" => true,
            "privacy_notice" => true
        ]);
    }
}
