<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PurchaseTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PurchaseTicketController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'store' => 'required|string',
            'total_amount' => 'required|numeric',
            'voucher' => 'required|file'
        ]);

        if($validator->fails()){
            return response()->json($validator->getMessageBag(), 400);
        }

        $voucherPath = $request->voucher->store('','voucher');

        $purchaseTicket = new PurchaseTicket();
        $purchaseTicket->fill($request->only([
            'store',
            'total_amount'
        ]));

        $purchaseTicket->user()->associate($request->user());
        $purchaseTicket->voucher = $voucherPath;
        $purchaseTicket->save();

        return response()->json($purchaseTicket, 200);

    }

    public function voucher(PurchaseTicket $purchaseTicket){

        Log::info("Ticket: $purchaseTicket->id");

        $voucherLocation = Storage::disk('voucher')->path($purchaseTicket->voucher);

        $voucher = File::get($voucherLocation);

        $voucherMimetype = File::mimeType($voucherLocation);

        return response($voucher)->header('Content-Type', $voucherMimetype);

    }

    public function index(Request $request){

        $validator = Validator::make($request->all(), [
            'page' => 'required|integer|min:0',
            'size' => 'required|integer|min:5',
            'filter' => 'string'
        ]);

        if($validator->fails()){
            return response()->json($validator->getMessageBag(), 400);
        }

        $query = $request->user()->purchaseTickets();

        $totalElements = $query->count();

        if($request->has('filter')){
            $query = $query->where('store', 'like', "%$request->filter%");
        }

        $totalFilteredElements = $query->count();
        $totalPages = ceil($totalFilteredElements / $request->size);

        $query = $query->limit($request->size);
        $query = $query->offset($request->page * $request->size);
        $query = $query->orderBy('created_at', 'DESC');

        return response()->json([
            "total_elements" => $totalElements,
            "total_filtered_elements" => $totalFilteredElements,
            "total_pages" => $totalPages,
            "current_page" => intval($request->page),
            "elements" => $query->get()
        ], 200);
    }

    public function show(PurchaseTicket $purchaseTicket){
        return response()->json($purchaseTicket, 200);
    }

}
