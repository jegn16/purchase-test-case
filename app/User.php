<?php

namespace App;

use App\Models\PurchaseTicket;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'username', 'phone_number', "birthday", 'password', 'terms_and_conditions', 'privacy_notice',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'terms_and_conditions', 'privacy_notice', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'terms_and_conditions' => 'boolean',
        'privacy_notice' => 'boolean',
    ];

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\User
     */
    public function findForPassport(string $username){
        return $this->where('username', $username)->first();
    }

    public function setPasswordAttribute(string $password){
        $this->attributes['password'] = Hash::make($password);
    }

    public function purchaseTickets(){
        return $this->hasMany(PurchaseTicket::class);
    }
}
