<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PurchaseTicket extends Model
{

    protected $fillable = [
        'store', 'total_amount', 'voucher', 'user_id'
    ];

    protected $hidden = [
        'updated_at'
    ];

    protected $casts = [
        'total_amount' => 'double'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
