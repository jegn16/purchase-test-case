<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function(){
    Route::prefix('oauth')->group(function(){

        Route::get('profile', 'SessionController@profile');
        Route::post('register', 'SessionController@register');
        Route::delete('logout', 'SessionController@logout');
    });

    Route::apiResource('purchase_tickets', 'PurchaseTicketController')->only([
        'index', 'store', 'show'
    ]);

    Route::get('purchase_tickets/{purchase_ticket}/voucher', 'PurchaseTicketController@voucher');
});
